import React from 'react';

function About(props) {
    return (
        <div id='about'>
            <div className="about-image">
                <img src={props.image} alt="" />
            </div>
            <div className="about-text">
                <h2>{props.title}</h2>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi sit eligendi 
                    dolores excepturi et quae culpa. Quaerat modi vel, id porro corrupti quisquam voluptates quas ullam, dolorum nulla impedit voluptatum?
                </p>
                <button>{props.button}</button>
            </div>
        </div>
        
    )
}

export default About;